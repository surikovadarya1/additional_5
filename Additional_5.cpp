﻿#include <iostream>


class Foo
{
public:
    Foo(int j) 
    { 
        std::cout << "Foo class: CONSTRUCTOR" << std::endl;
        i = new int[j]; 
        size = j;
        for (int k = 0; k < j; k++)
        {
            i[k] = k;
        }
    }
    
    virtual ~Foo() 
    { 
        std::cout << "Foo class: DESTRUCTOR" << std::endl;
        delete[] i; 
    }
    
protected:
    int* i;
    int size;
};

class Bar : public Foo
{
public:
    Bar(int j) : Foo (j)
    { 
        std::cout << "Bar class: CONSTRUCTOR" << std::endl;
        i = new char[j]; 
        size = j;
        for (int k = 0; k < j; k++)
        {
            i[k] = j;
        }
    }
    Bar* copy() const
    {
        Bar* k = new Bar(*this);
        return k;
    }
    virtual ~Bar() 
    { 
        std::cout << "Bar class: DESTRUCTOR" << std::endl;
        delete[] i; 
    }

protected:
    Bar(const Bar& other) : Foo(other)
    {
        size = other.size;
        i = new char[other.size];
        for (int k = 0; k < other.size; k++)
        {
            i[k] = other.i[k];
        }

    }
    char* i;
    int size;
};


int main()
{
    Bar* b = new Bar(10);
    Foo* f = b->copy();
    delete f;




}



